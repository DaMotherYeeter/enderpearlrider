package damotheryeeter.general.enderpearlride;

import damotheryeeter.general.enderpearlride.events.*;

import org.bukkit.plugin.java.JavaPlugin;

public class EnderPearlRide extends JavaPlugin {
    
    @Override
    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new EnderThrowEvent(), this);
    }
}
