package damotheryeeter.general.enderpearlride.events;

import org.bukkit.entity.Player;
import static org.bukkit.Material.ENDER_PEARL;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;

public class EnderThrowEvent implements Listener {

    @EventHandler
    public void onThrow(final ProjectileLaunchEvent e) {
        if (e.getEntityType().equals(EntityType.ENDER_PEARL)) {
            if (!(e.getEntity().getShooter() instanceof Player)) return;
            if (((Player) e.getEntity().getShooter()).hasPermission("enderpearlride.ride")) {
                e.getEntity().setPassenger((Player) e.getEntity().getShooter());
                if (((Player) e.getEntity().getShooter()).hasPermission("enderpearlride.infinite")) {
                    final Player p = (Player) e.getEntity().getShooter();
                    final ItemStack[] pearl = {new ItemStack(ENDER_PEARL, 1)};
                    p.getInventory().addItem(pearl);                
                }
            }
        }
    }

}
